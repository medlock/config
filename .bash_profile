#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# Set path
# (sdk - run via wine)
export PATH="$PATH:/home/cmm/ConnectIQ/Sdks/connectiq-sdk-win-4.0.6-2021-10-06-af9b9d6e2"
