#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# alias ls='ls --color=auto'
# PS1='[\u@\h \W]\$ '


alias ll='ls -al'
alias svcl='pstree -p | grep runsv'
alias mutt='/usr/bin/neomutt'

alias graph='git log --all --decorate --oneline --graph'
alias tabs='xsetwacom set 10 MapToOutput eDP1'	
alias tabu='xsetwacom set 10 MapToOutput desktop'

alias hdmi-l='xrandr --output HDMI2 --auto --left-of eDP1'

alias dpms='xset s off -dpms'
